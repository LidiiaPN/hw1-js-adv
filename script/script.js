"use strict"
/*Теоретичне питання

1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
Концепція прототипів, яка використовується в JavaScript, проста. Якщо об'єкт B є прототипом об'єкта A,
 то всякий раз, коли у B є властивість, наприклад, колір, A  успадкує той же самий колір, якщо інше не
  вказано явно. І нам не потрібно повторювати всю інформацію про A, яку він успадковує від  B.
  Наслідування у JavaScript реалізується єдиною конструкцією: objects . Кожен object має внутрішнє посилання
   на інший object , що є його прототипом ( prototype ). 
2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
super - це ключове слово, яке використовується для виклику методів батьківського класу в дочірньому класі. Коли
 ми розширюємо клас за допомогою наслідування, іноді нам потрібно використовувати або модифікувати поведінку батьківського класу,
  замість того, щоб писати все заново.
super можна використовувати в будь-якому дочірньому класі, що наслідує інший клас. Воно зазвичай використовується в конструкторі
 дочірнього класу для виклику конструктора батьківського класу.

Завдання

- Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так,
 щоб ці характеристики заповнювалися під час створення об'єкта.
- Створіть гетери та сеттери для цих властивостей.
- Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
- Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
- Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.*/

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get lang() {
    return this._lang;
  }

  set lang(value) {
    this._lang = value;
  }

  get salary() {
    return super.salary * 3;
  }
}

const programmer1 = new Programmer('Bob', 30, 10000, ['JavaScript']);
const programmer2 = new Programmer('Anna', 25, 12000, ['Python', 'Java']);
const programmer3 = new Programmer('Mark', 45, 22000, ['JavaScript', 'Java']);

console.log("Programmer 1:");
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", programmer1.salary);
console.log("Languages:", programmer1.lang);

console.log("\Programmer 2:");
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", programmer2.salary);
console.log("Languages:", programmer2.lang);

console.log("\Programmer 3:");
console.log("Name:", programmer3.name);
console.log("Age:", programmer3.age);
console.log("Salary:", programmer3.salary);
console.log("Languages:", programmer3.lang);


//вар 2 з отриманням даних за допомогою prompt
/*
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get lang() {
    return this._lang;
  }

  set lang(value) {
    this._lang = value;
  }

  get salary() {
    return this._salary * 3; 
  }
};


let name1 = prompt("Enter name for programmer 1:");
let age1 = prompt("Enter age for programmer 1:");
let salary1 = prompt("Enter salary for programmer 1:");
let lang1 = prompt("Enter programming languages for programmer 1 separated by comma:").split(",");

let programmer1 = new Programmer(name1, parseInt(age1), parseFloat(salary1), lang1);

let name2 = prompt("Enter name for programmer 2:");
let age2 = prompt("Enter age for programmer 2:");
let salary2 = prompt("Enter salary for programmer 2:");
let lang2 = prompt("Enter programming languages for programmer 2 separated by comma:").split(",");


let programmer2 = new Programmer(name2, parseInt(age2), parseFloat(salary2), lang2);


console.log("Programmer 1:");
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", programmer1.salary);
console.log("Languages:", programmer1.lang);

console.log("\nProgrammer 2:");
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", programmer2.salary);
console.log("Languages:", programmer2.lang);*/